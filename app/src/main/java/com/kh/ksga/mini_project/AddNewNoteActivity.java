package com.kh.ksga.mini_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.material.textfield.TextInputEditText;

public class AddNewNoteActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_cancel, btn_save;
    TextInputEditText et_title, et_description;

    String title, description;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_note);

        intent = getIntent();

        btn_cancel = findViewById(R.id.button_cancel);
        btn_save = findViewById(R.id.button_save);

        et_title = findViewById(R.id.et_title);
        et_description = findViewById(R.id.et_description);

        btn_save.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_save:
//                title = et_title.getText().toString();
//                description = et_description.getText().toString();
                if(title.isEmpty() || description.isEmpty()){
                    YoYo.with(Techniques.BounceIn).repeat(5).duration(1000).playOn(et_title);
                    YoYo.with(Techniques.BounceIn).repeat(5).duration(1000).playOn(et_description);
                }else{
                    intent.putExtra("title",et_title.getText().toString());
                    intent.putExtra("description",et_description.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.button_cancel:
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
            default:
                Toast.makeText(AddNewNoteActivity.this,"No Contact",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}