package com.kh.ksga.mini_project;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.kh.ksga.mini_project.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityMainBinding binding;
    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 1. initialize view binding object
        binding = ActivityMainBinding.inflate(getLayoutInflater());

        // 2. define view object
        View view = binding.getRoot();

        // 3. set view into contentView
        setContentView(view);

        binding.gallery.setOnClickListener(this);
        binding.contact.setOnClickListener(this);
        binding.notebook.setOnClickListener(this);
        binding.call.setOnClickListener(this);
        binding.profile.setOnClickListener(this);

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gallery:
                intent.setClass(this, GalleryScreenActivity.class);
                startActivity(intent);
                break;
            case R.id.contact:
                intent.setClass(this, ContactActivity.class);
                startActivity(intent);
                break;
            case R.id.notebook:
                intent.setClass(this, NotebookActivity.class);
                startActivity(intent);
                break;
            case R.id.call:
                intent.setClass(this, CallActivity.class);
                startActivity(intent);
                break;
            case R.id.profile:
                intent.setClass(this, ProfileActivity.class);
                startActivity(intent);
                break;
            default:
                Toast.makeText(this, "Please Choose Category", Toast.LENGTH_SHORT).show();
        }

    }


}