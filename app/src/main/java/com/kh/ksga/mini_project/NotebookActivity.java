package com.kh.ksga.mini_project;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class NotebookActivity extends AppCompatActivity {

    Intent intent;
    FrameLayout addItem;
    DynamicView dynamicView;
    Context context;
    int i = 1, j = 0;
    private GridLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notebook);

        rootLayout = findViewById(R.id.controller_note);
        addItem = findViewById(R.id.add_item);

        addItem.setOnClickListener(view -> {
            intent = new Intent(NotebookActivity.this, AddNewNoteActivity.class);
            startActivityForResult(intent, 1);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                final String title = data.getStringExtra("title");
                final String description = data.getStringExtra("description");
                dynamicView = new DynamicView(context);
                GridLayout.Spec column = GridLayout.spec(1);
                GridLayout.Spec row = GridLayout.spec(0);
                rootLayout.addView(dynamicView.frame(getApplicationContext(), title, description), new GridLayout.LayoutParams(row, column));
                Toast.makeText(this, "Title : " + title + "\n Description : " + description, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Result: no item", Toast.LENGTH_LONG).show();
            }
        }
    }
}